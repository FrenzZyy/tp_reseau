# TP3 : Progressons vers le réseau d'infrastructure
# I. (mini)Architecture réseau

🌞 **Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme** :

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.0.128`        | `255.255.255.192` | 62                           | `10.3.0.190`         | `10.3.0.191`                                                                                   |
| `server1`     | `10.3.0.0`        | `255.255.255.128` | 126                           | `10.3.0.126`         | `10.3.0.127`                                                                                   |
| `server2`     | `10.3.0.192`        | `255.255.255.240` | 14                           | `10.3.0.206`         | `10.3.0.207`                                                                                   |

🌞 **Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :**

- il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle
```
[admin@router ~]$ ip a

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:78:78:ae brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.190/26 brd 10.3.0.191 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe78:78ae/64 scope link
       valid_lft forever preferred_lft forever

4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:61:73:47 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.126/25 brd 10.3.0.127 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe61:7347/64 scope link
       valid_lft forever preferred_lft forever

5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:67:ee:27 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.206/28 brd 10.3.0.207 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe67:ee27/64 scope link
       valid_lft forever preferred_lft forever
```
- il a un accès internet

```
[admin@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=28.8 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=26.3 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=24.9 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=27.1 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 24.883/26.745/28.783/1.415 ms
```
- il a de la résolution de noms

```
[admin@router ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 14412
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       142.250.179.78

;; Query time: 58 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Tue Oct 19 10:07:49 CEST 2021
;; MSG SIZE  rcvd: 55
```
- il porte le nom `router.tp3`*

```
[admin@router ~]$ cat /etc/hostname
router.tp3

```
- n'oubliez pas [d'activer le routage sur la machine](../../cours/memo/rocky_network.md#activation-du-routage)

```
[admin@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
```
---

🌞 **Vous remplirez aussi** au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, **le 🗃️ tableau d'adressage 🗃️ suivant :**

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.0.190/26`         | `10.3.0.126/25`         | `10.3.0.206/28`         | Carte NAT             |
| ...          | ...                  | ...                  | ...                  | `10.3.?.?/?`          |

> *N'oubliez pas de **TOUJOURS** fournir le masque quand vous écrivez une IP.*

# II. Services d'infra
## 1. Serveur DHCP

You already did this in TP 2. Repeat.

🌞 **Mettre en place une machine qui fera office de serveur DHCP** 

- donner une IP aux machines clients qui le demande
- leur donner l'adresse de leur passerelle
- leur donner l'adresse d'un DNS utilisable

**Voir le fichier httpd.conf**

📁 **Fichier `dhcpd.conf`**

---

🌞 **Mettre en place un client dans le réseau `client1`**

- la machine récupérera une IP dynamiquement grâce au serveur DHCP

```
[admin@marcel ~]$ sudo nmcli con up enp0s8
[sudo] password for admin:
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/2)
```
```
[admin@marcel ~]$ ip a
{...}
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e5:c3:8f brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.130/26 brd 10.3.0.191 scope global dynamic noprefixroute enp0s8
       valid_lft 484sec preferred_lft 484sec
    inet6 fe80::a00:27ff:fee5:c38f/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
- ainsi que sa passerelle et une adresse d'un DNS utilisable

```
[admin@marcel ~]$ ip r s
default via 10.3.0.190 dev enp0s8 proto dhcp metric 100
10.3.0.128/26 dev enp0s8 proto kernel scope link src 10.3.0.130 metric 100
```
```
[admin@marcel ~]$ dig google.com

{...}
;; ANSWER SECTION:
google.com.             276     IN      A       216.58.215.46

;; Query time: 25 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Oct 17 21:11:42 CEST 2021
;; MSG SIZE  rcvd: 55
```

🌞 **Depuis `marcel.client1.tp3`**

- prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP

```
[admin@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=26.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=18.3 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=22.5 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=112 time=31.2 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3009ms
rtt min/avg/max/mdev = 18.319/24.525/31.180/4.722 ms
```

```
[admin@marcel ~]$ dig google.com

{...}
;; ANSWER SECTION:
google.com.             276     IN      A       216.58.215.46

;; Query time: 25 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Oct 17 21:11:42 CEST 2021
;; MSG SIZE  rcvd: 55
```


- à l'aide de la commande `traceroute`, prouver que `marcel.client1.tp3` passe par `router.tp3` pour sortir de son réseau

```
[admin@marcel ~]$ traceroute google.com
traceroute to google.com (216.58.206.238), 30 hops max, 60 byte packets
 1  _gateway (10.3.0.190)  17.949 ms  16.224 ms  15.086 ms
 2  10.0.2.2 (10.0.2.2)  12.056 ms  11.808 ms  11.048 ms
 {...}
```
on voit bien que marcel passe par l'ip du routeur qui est 10.3.0.190

## 2. Serveur DNS
### B. SETUP copain

🌞 **Mettre en place une machine qui fera office de serveur DNS**

**voir le fichier named.conf**

🌞 **Tester le DNS depuis `marcel.client1.tp3`**

- essayez une résolution de nom avec `dig`
```
[admin@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 4071
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: fc035919da0fe8f95ecd099f616c8db878c75f409de6b1e4 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       172.217.22.142

;; AUTHORITY SECTION:
google.com.             170742  IN      NS      ns2.google.com.
google.com.             170742  IN      NS      ns3.google.com.
google.com.             170742  IN      NS      ns1.google.com.
google.com.             170742  IN      NS      ns4.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         170742  IN      A       216.239.34.10
ns1.google.com.         170742  IN      A       216.239.32.10
ns3.google.com.         170742  IN      A       216.239.36.10
ns4.google.com.         170742  IN      A       216.239.38.10
ns2.google.com.         170742  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         170742  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         170742  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         170742  IN      AAAA    2001:4860:4802:38::a

;; Query time: 30 msec
;; SERVER: 10.3.0.11#53(10.3.0.11)
;; WHEN: Sun Oct 17 22:55:21 CEST 2021
;; MSG SIZE  rcvd: 331
```

- prouvez que c'est bien votre serveur DNS qui répond pour chaque `dig`

```
[admin@marcel ~]$ dig google.com @10.3.0.11

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com @10.3.0.11
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 44922
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: b31af136226f3d5a062f4adf616c8b6a1bbda36e02fa8c23 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             183     IN      A       172.217.22.142

;; AUTHORITY SECTION:
google.com.             171332  IN      NS      ns1.google.com.
google.com.             171332  IN      NS      ns4.google.com.
google.com.             171332  IN      NS      ns2.google.com.
google.com.             171332  IN      NS      ns3.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         171332  IN      A       216.239.34.10
ns1.google.com.         171332  IN      A       216.239.32.10
ns3.google.com.         171332  IN      A       216.239.36.10
ns4.google.com.         171332  IN      A       216.239.38.10
ns2.google.com.         171332  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         171332  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         171332  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         171332  IN      AAAA    2001:4860:4802:38::a

;; Query time: 3 msec
;; SERVER: 10.3.0.11#53(10.3.0.11)
;; WHEN: Sun Oct 17 22:45:31 CEST 2021
;; MSG SIZE  rcvd: 331
```
on peut voir aussi a la 3e ligne avant la fin que c'est bien mon server dns qui repond

Par exemple, vous devriez pouvoir exécuter les commandes suivantes :

```bash
$ dig marcel.client1.tp3
$ dig dhcp.client1.tp3
```

> Pour rappel, avec `dig`on peut préciser directement sur la ligne de commande à quel serveur poser la question avec  le caractère `@`. Par exemple `dig google.com @8.8.8.8` permet de préciser explicitement qu'on veut demander à `8.8.8.8` à quelle IP se trouve `google.com`.

⚠️ **NOTE : A partir de maintenant, vous devrez modifier les fichiers de zone pour chaque nouvelle machine ajoutée (voir [📝**checklist**📝](#0-prérequis).**

🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds

- les serveurs, on le fait à la main
```
[admin@web1] sudo /etc/resolv.conf
nameserver 10.3.0.11
```
- les clients, c'est fait *via* DHCP

pour les client voir le fichier dhcpd.conf

## 3. Get deeper

On va affiner un peu la configuration des outils mis en place.

### A. DNS forwarder

🌞 **Affiner la configuration du DNS**

- faites en sorte que votre DNS soit désormais aussi un forwarder DNS
- c'est à dire que s'il ne connaît pas un nom, il ira poser la question à quelqu'un d'autre

> Hint : c'est la clause `recursion` dans le fichier `/etc/named.conf`. Et c'est déjà activé par défaut en fait.

🌞 Test !

- vérifier depuis `marcel.client1.tp3` que vous pouvez résoudre des noms publics comme `google.com` en utilisant votre propre serveur DNS (commande `dig`)
- pour que ça fonctionne, il faut que `dns1.server1.tp3` soit lui-même capable de résoudre des noms, avec `1.1.1.1` par exemple

### B. On revient sur la conf du DHCP

🌞 **Affiner la configuration du DHCP**

- faites en sorte que votre DHCP donne désormais l'adresse de votre serveur DNS aux clients
```
[admin@dns1 ~]$ sudo cat /etc/named.conf
{...}
        recursion yes;
{...}
```
---

# III. Services métier
## 1. Serveur Web

🌞 **Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients**

```
[admin@web1 ~]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

```

🌞 **Test test test et re-test**

- testez que votre serveur web est accessible depuis `marcel.client1.tp3`
  - utilisez la commande `curl` pour effectuer des requêtes HTTP

```
[admin@marcel ~]$ curl 10.3.0.194
curl: (7) Failed to connect to 10.3.0.194 port 80: No route to host
[admin@marcel ~]$ curl 10.3.0.194:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

## 2. Partage de fichiers
### B. Le setup wola

🌞 **Setup d'une nouvelle machine, qui sera un serveur NFS**

```
[admin@nfs1 ~]$ sudo dnf -y install nfs-utils
[admin@nfs1 ~]$ sudo nano /etc/idmapd.conf
[admin@nfs1 ~]$ sudo nano /etc/exports
[admin@nfs1 ~]$ sudo mkdir /srv/nfs_share
[admin@nfs1 srv]$ sudo systemctl enable rpcbind nfs-server
[sudo] password for admin:
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[admin@nfs1 srv]$ sudo firewall-cmd --add-service=nfs --permanent
success
```

🌞 **Configuration du client NFS**

```
[admin@web1 system]$ sudo dnf -y install nfs-utils
[admin@web1 system]$ sudo nano /etc/idmapd.conf
[admin@web1 srv]$ sudo mount -t nfs 10.3.0.195:/srv/nfs_share /srv/nfs
```

🌞 **TEEEEST**

```
[admin@nfs1 ~]$ sudo touch /srv/nfs/test2
[admin@web1 ~]$ sudo ls -la /srv/nfs/
total 0
drwxrw-rw-. 2 root root 31  3 oct.  19:52 .
drwxr-xr-x. 3 root root 17  3 oct.  18:54 ..
-rw-r--r--. 1 root root  0  3 oct.  18:35 test
-rw-r--r--. 1 root root  0  3 oct.  19:52 test2
```

# IV. Un peu de théorie : TCP et UDP

Bon bah avec tous ces services, on a de la matière pour bosser sur TCP et UDP. P'tite partie technique pure avant de conclure.

🌞 **Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP :**

Les requête HTTP, NFS et SSH sont encapsulés dans des trames TCP, uniquement les trames DNS sont encapsulés dans des trames UDP

📁 **Captures réseau `tp3_ssh.pcap`, `tp3_http.pcap`, `tp3_dns.pcap` et `tp3_nfs.pcap`**

> **Prenez le temps** de réfléchir à pourquoi on a utilisé TCP ou UDP pour transmettre tel ou tel protocole. Réfléchissez à quoi servent chacun de ces protocoles, et de ce qu'on a besoin qu'ils réalisent.

🌞 **Expliquez-moi pourquoi je ne pose pas la question pour DHCP.**

🌞 **Capturez et mettez en évidence un *3-way handshake***

📁 **Capture réseau `tp3_3way.pcap`**

# V. El final

🌞 **Bah j'veux un schéma.**

- réalisé avec l'outil de votre choix
- un schéma clair qui représente
  - les réseaux
    - les adresses de réseau devront être visibles
  - toutes les machines, avec leurs noms
  - devront figurer les IPs de toutes les interfaces réseau du schéma
  - pour les serveurs : une indication de quel port est ouvert
- vous représenterez les host-only comme des switches
- dans le rendu, mettez moi ici à la fin :
  - le schéma
  - le 🗃️ tableau des réseaux 🗃️
  - le 🗃️ tableau d'adressage 🗃️
    - on appelle ça aussi un "plan d'adressage IP" :)

> J'vous le dis direct, un schéma moche avec Paint c'est -5 Points. Je vous recommande [draw.io](http://draw.io).

🌞 **Et j'veux des fichiers aussi, tous les fichiers de conf du DNS**

- 📁 Fichiers de zone
- 📁 Fichier de conf principal DNS `named.conf`
- faites ça à peu près propre dans le rendu, que j'ai plus qu'à cliquer pour arriver sur le fichier ce serait top