# TP2 : On va router des trucs
## I. ARP
### 1. Echange ARP

**🌞Générer des requêtes ARP**
- effectuer un ping d'une machine à l'autre

```
[admin@node1 ~]$ ping 10.2.1.12

PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=2.35 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=7.92 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=6.64 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=5.40 ms
64 bytes from 10.2.1.12: icmp_seq=5 ttl=64 time=6.40 ms
^C
--- 10.2.1.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4018ms
rtt min/avg/max/mdev = 2.345/5.741/7.921/1.878 ms
```
- observer les tables ARP des deux machines


```
[admin@node1 ~]$ arp -a
? (10.2.1.12) at 08:00:27:69:b4:41 [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:0c [ether] on enp0s8
```

```
[admin@node2 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:0c [ether] on enp0s8
? (10.2.1.11) at 08:00:27:ba:94:d1 [ether] on enp0s8
```
- repérer l'adresse MAC de node1 dans la table ARP de node2 et vice-versa

l'adresse MAC de node1 est 08:00:27:ba:94:d1 et l'adresse MAC de node2 est 08:00:27:69:b4:41

- prouvez que l'info est correcte

```
[admin@node1 ~]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.2.1.12                ether   08:00:27:69:b4:41   C                     enp0s8
10.2.1.1                 ether   0a:00:27:00:00:0c   C                     enp0s8
```

```
[admin@node2 ~]$ ip a

{...}
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:69:b4:41 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe69:b441/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

### 2. Analyse de trames

**🌞Analyse de trames**

- utilisez la commande tcpdump pour réaliser une capture de trame

```
[admin@localhost ~]$ sudo tcpdump -i enp0s8 -c 10 -w tp2_arp.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
10 packets captured
11 packets received by filter
0 packets dropped by kernel
```

- videz vos tables ARP, sur les deux machines, puis effectuez un ping

```
[admin@node1 ~]$ sudo ip neigh flush all
[sudo] password for admin:
[admin@node1 ~]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.2.1.1                 ether   0a:00:27:00:00:0c   C                     enp0s8
```

```
[admin@node2 ~]$ sudo ip neigh flush all
[sudo] password for admin:
[admin@node2 ~]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.2.1.1                 ether   0a:00:27:00:00:0c   C                     enp0s8
```

- écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, je veux TOUTES les trames

| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Requête ARP | `node1` `08:00:27:ba:94:d1` | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `node2` `08:00:27:69:b4:41` | `node1` `08:00:27:ba:94:d1`   |

## II. Routage

### 1. Mise en place du routage

- Activer le routage sur le noeud router.net2.tp2

```
[admin@router ~]$ sudo firewall-cmd --list-all
[sudo] password for admin:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[admin@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9
[admin@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[admin@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```
- Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping

```
[admin@node1 ~]$ ip route show
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
10.2.2.0/24 via 10.2.1.254 dev enp0s8 proto static metric 100
```

```
[admin@marcel]$ ip route show
default via 10.2.2.1 dev enp0s8 proto static metric 100
10.2.1.0/24 via 10.2.2.254 dev enp0s8 proto static metric 100
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```

```
[admin@router ~]$ ip route show
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.254 metric 101
10.2.2.0/24 dev enp0s9 proto kernel scope link src 10.2.2.254 metric 102
```

```
[admin@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.87 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.94 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.97 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.85 ms
64 bytes from 10.2.2.12: icmp_seq=5 ttl=63 time=1.97 ms
^C
--- 10.2.2.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4009ms
rtt min/avg/max/mdev = 1.848/1.917/1.970/0.058 ms
```

## 2. Analyse de trames

- Analyse des échanges ARP

table arp de node1 

```
[admin@node1 ~]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.2.1.254               ether   08:00:27:71:5c:d2   C                     enp0s8
10.2.1.1                 ether   0a:00:27:00:00:0c   C                     enp0s8
```

table arp de marcel 

```
[admin@lmarcel]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
_gateway                 ether   0a:00:27:00:00:04   C                     enp0s8
10.2.2.254               ether   08:00:27:67:2f:e5   C                     enp0s8
```

table arp du routeur 

```
[admin@router ~]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.2.1.11                ether   08:00:27:ba:94:d1   C                     enp0s8
10.2.2.12                ether   08:00:27:69:b4:41   C                     enp0s9
10.2.1.1                 ether   0a:00:27:00:00:0c   C                     enp0s8
_gateway                 ether   52:54:00:12:35:02   C                     enp0s3
```

on peut constater que les trames arp d'un meme reseau communique uniquement avec le routeur

| ordre | type trame  | ip source   | source                      | ip destination | destination                 |
|-------|-------------|-------------|-----------------------------|----------------|-----------------------------|
| 1     | Requête ARP | x           | `node1` `08:00:27:ba:94:d1` | x              | Broadcast `FF:FF:FF:FF:FF`  |
| 2     | Réponse ARP | x           | `router` `08:00:27:71:5c:d2`| x              | `node1` `08:00:27:ba:94:d1` |
| 3     | Requête ARP | x           | `router` `08:00:27:67:2f:e5`| x              | Broadcast `FF:FF:FF:FF:FF`  |
| 4     | Réponse ARP | x           | `marcel` `08:00:27:69:b4:41`| x              | `router` `08:00:27:67:2f:e5`|
| 5     | Ping        | 10.2.1.11   | `node1` `08:00:27:ba:94:d1` | 10.2.2.12      | `marcel` `08:00:27:69:b4:41`|
| 6     | Pong        | 10.2.2.12   | `marcel` `08:00:27:69:b4:41`| 10.2.1.11      | `node1` `08:00:27:ba:94:d1` |

## 3. Accès internet

- Donnez un accès internet à vos machines

# **node1**

```
[admin@node1 ~]$ sudo nano /etc/sysconfig/network

GATEWAY=10.2.1.254
```

```
[admin@node1 ~]$ ip r s
default via 10.2.1.254 dev enp0s8 proto static metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
10.2.2.0/24 via 10.2.1.254 dev enp0s8 proto static metric 100
```

```
[admin@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=111 time=49.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=111 time=51.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=111 time=38.7 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=111 time=45.1 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 38.665/46.185/51.596/4.936 ms
```

```
[admin@node1 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 54572
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             165     IN      A       142.250.75.238

;; Query time: 51 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Sep 24 07:07:02 CEST 2021
;; MSG SIZE  rcvd: 55
```

```
[admin@node1 ~]$ ping google.com

PING google.com (216.58.198.206) 56(84) bytes of data.
64 bytes from par10s27-in-f206.1e100.net (216.58.198.206): icmp_seq=1 ttl=109 time=52.0 ms
64 bytes from par10s27-in-f206.1e100.net (216.58.198.206): icmp_seq=2 ttl=109 time=50.2 ms
64 bytes from par10s27-in-f206.1e100.net (216.58.198.206): icmp_seq=3 ttl=109 time=47.2 ms
64 bytes from par10s27-in-f206.1e100.net (216.58.198.206): icmp_seq=4 ttl=109 time=44.1 ms
64 bytes from par10s27-in-f206.1e100.net (216.58.198.206): icmp_seq=5 ttl=109 time=78.2 ms
^C
--- google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4009ms
rtt min/avg/max/mdev = 44.144/54.345/78.186/12.216 ms
```

# **marcel**

```
[admin@node1 ~]$ sudo nano /etc/sysconfig/network

GATEWAY=10.2.2.254
```

```
[admin@marcel ~]$ ip r s
default via 10.2.2.254 dev enp0s8 proto static metric 100
10.2.1.0/24 via 10.2.2.254 dev enp0s8 proto static metric 100
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```

```
[admin@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=111 time=35.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=111 time=63.9 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=111 time=58.3 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=111 time=57.7 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 35.853/53.950/63.912/10.727 ms
```

```
[admin@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 38631
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             248     IN      A       216.58.214.78

;; Query time: 53 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Sep 24 04:45:12 CEST 2021
;; MSG SIZE  rcvd: 55
```

```
[admin@marcel ~]$ ping google.com

PING google.com (142.250.75.238) 56(84) bytes of data.
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=1 ttl=110 time=45.0 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=2 ttl=110 time=53.7 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=3 ttl=110 time=52.9 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=4 ttl=110 time=41.2 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=5 ttl=110 time=40.7 ms
^C
--- google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4008ms
rtt min/avg/max/mdev = 40.713/46.714/53.740/5.600 ms
```
| ordre | type trame  | ip source   | source                      | ip destination | destination                 |
|-------|-------------|-------------|-----------------------------|----------------|-----------------------------|
| 1     | Ping        | 10.2.1.11   | `node1` `08:00:27:ba:94:d1` | 8.8.8.8      | `google` `08:00:27:71:5c:d2`|
| 2     | Pong        | 8.8.8.8   | `google` `08:00:27:71:5c:d2`| 10.2.1.11      | `node1` `08:00:27:ba:94:d1` |

on peut constater que le routeur ne connait pas l'adresse MAC de destination (google) donc il va mettre sa propre adresse MAC a la place (celle de l'interface qui est connecter au reseau d'ou vien le ping (il utilise celle de reseau 1 comme le ping vien de node1))

# III. DHCP

## 1. Mise en place du serveur DHCP

- installation du serveur sur node1.net1.tp2

```
[admin@node1 ~]$ sudo dnf -y install dhcp-server
```

```
[admin@node1 ~]$ sudo nano /etc/dhcp/dhcpd.conf

# DHCP Server Configuration file

# specify domain name
option domain-name     "tp2srv.dhcp";
# specify DNS server's hostname or IP address
option domain-name-servers     8.8.8.8;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.2.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.2.1.2 10.2.1.253;
    # specify broadcast address
    option broadcast-address 10.2.1.255;
    # specify gateway
    option routers 10.2.1.254;
}
```

```
[admin@node2 ~]$ ip a

{...}
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ba:28:a3 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.2/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 396sec preferred_lft 396sec
    inet6 fe80::a00:27ff:feba:28a3/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

```
[admin@marcel ~]$ sudo nmcli con reload
[admin@marcel ~]$ sudo nmcli con up enp0s8
Connection successfully activated
```
- vérifier avec une commande qu'il a récupéré son IP

```
[admin@node2 ~]$ ip a

{...}
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ba:28:a3 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.3/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 396sec preferred_lft 396sec
    inet6 fe80::a00:27ff:feba:28a3/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
- vérifier qu'il peut ping sa passerelle

```
[admin@node2 ~]$ ping 10.2.1.254

PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=1.83 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=1.11 ms
64 bytes from 10.2.1.254: icmp_seq=3 ttl=64 time=0.952 ms
64 bytes from 10.2.1.254: icmp_seq=4 ttl=64 time=1.01 ms
^C
--- 10.2.1.254 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 0.952/1.223/1.827/0.353 ms
```
- vérifier la présence de la route avec une commande

```
[admin@node2 ~]$ ip r s
default via 10.2.1.254 dev enp0s8 proto dhcp metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.3 metric 100
```
- vérifier que la route fonctionne avec un ping vers une IP

```
[admin@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=110 time=225 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=110 time=384 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=110 time=114 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=110 time=464 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 114.166/296.701/463.823/135.979 ms
```

```
[admin@node2 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.72 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=2.22 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.90 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.86 ms
^C
--- 10.2.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 1.724/1.925/2.216/0.182 ms
```
- vérifier avec la commande dig que ça fonctionne

```
[admin@node2 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 18792
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             1       IN      A       142.250.201.174

;; Query time: 59 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Sun Sep 26 17:14:51 CEST 2021
;; MSG SIZE  rcvd: 55
```
- vérifier un ping vers un nom de domaine

```
[admin@node2 ~]$ ping google.com

PING google.com (216.58.213.142) 56(84) bytes of data.
64 bytes from par21s03-in-f142.1e100.net (216.58.213.142): icmp_seq=1 ttl=111 time=34.4 ms
64 bytes from par21s03-in-f142.1e100.net (216.58.213.142): icmp_seq=2 ttl=111 time=26.10 ms
64 bytes from par21s03-in-f142.1e100.net (216.58.213.142): icmp_seq=3 ttl=111 time=32.1 ms
64 bytes from par21s03-in-f142.1e100.net (216.58.213.142): icmp_seq=4 ttl=111 time=667 ms
^C
--- google.com ping statistics ---
5 packets transmitted, 4 received, 20% packet loss, time 4006ms
rtt min/avg/max/mdev = 26.995/190.047/666.628/275.167 ms
```

## 2. Analyse de trames

| ordre | type trame  | ip source   | source                      | ip destination | destination                 |
|-------|-------------|-------------|-----------------------------|----------------|-----------------------------|
| 1     | DHCP Release | 10.2.1.3    | `node2` `08:00:27:ba:28:a3` | 10.2.1.11     | `node1` `08:00:27:ba:94:d1`|
| 2     | DHCP Discover | 0.0.0.0    | `node2` `08:00:27:ba:28:a3`| 255.255.255.255 | `Broadcast` `FF:FF:FF:FF:FF` |
| 3     | DHCP Offer | 10.2.1.11     | `node1` `08:00:27:ba:94:d1`| 10.2.1.3        | `node2` `08:00:27:ba:28:a3`|
| 4     | DHCP Request | 0.0.0.0     | `node2` `08:00:27:ba:28:a3`| 255.255.255.255| `Broadcast` `FF:FF:FF:FF:FF`|
| 5     | DHCP ACK     | 10.2.1.11   | `node1` `08:00:27:ba:94:d1` | 10.2.1.3      | `node2` `08:00:27:ba:28:a3`|
| 6     | Requête ARP| x | `node2` `08:00:27:ba:28:a3`| x     | `Broadcast` `FF:FF:FF:FF:FF` |
| 7     | Requête ARP| x  | `node2` `08:00:27:ba:28:a3`| x     | `Broadcast` `FF:FF:FF:FF:FF` |
| 8     | Requête ARP| x  | `node1` `08:00:27:ba:94:d1`| x     | `node2` `08:00:27:ba:28:a3` |
| 9     | Réponse ARP| x  | `node2` `08:00:27:ba:28:a3`| x     | `node1` `08:00:27:ba:94:d1` |