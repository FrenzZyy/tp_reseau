# TP1 - Mise en jambes
## I. Exploration local en solo
### 1. Affichage d'informations sur la pile TCP/IP locale

- **Affichez les infos des cartes réseau de votre PC**

pour récuperer l'adresse ip et l'adresse MAC on utilise la commande **ipconfig /all**.

``` 
{...}
Carte réseau sans fil Wi-Fi :
    
    {...}
   Adresse physique . . . . . . . . . . . : E6-AA-68-6E-29-88
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.151(préféré)

Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Adresse physique . . . . . . . . . . . : 08-97-98-94-9F-57
   {...} 
```

 on peut observer que lors de l'executuion de la commande on obtient aucune adresse ip pour la carte ethernet et c'est normal car ma carte ethernet n'est connecter a aucun reseau on reçois juste l'adresse MAC car elle propre a chaque carte reseau
 
- **Affichez votre gateway**

pour obtenir la gateway de la carte wifi on utilise la commande **ipconfig** qui nous donne la passerelle par defaut (10.33.3.253)
```
Carte réseau sans fil Wi-Fi :
    
    {...}
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
    {...}
```

- **Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

pour obtenir l'adresse MAC, IP et la gateway je me suis rendu dans le panneau de configuration puis centre réseau et partage ensuite on fait modifier les parametre de la carte pusi j'ai selectionner ma carte wifi et j'ai fait clique droit statut puis details et j'ai obtenue le resultat ci-dessous. L'adresse MAC est E6-AA-68-6E-29-88, l'adresse IP est 10.33.3.151 et la passerelle est 10.33.3.253

![](https://i.imgur.com/CP8U0AI.png)

- **à quoi sert la gateway dans le réseau d'YNOV ?**

La passerelle dans le reseau d'Ynov permet d'aller sur 

### 2. Modifications des informations

#### A. Modification d'adresse IP (part 1)

![](https://i.imgur.com/vgjjnS0.png)

![](https://i.imgur.com/DCpGe0A.png)

#### B. Table ARP

- **Exploration de la table ARP**

Pour afficher la table arp j'ai utilisé la commande arp -a, ensuite pour identifier l'adresse MAC de la passerelle du reseau j'ai rechercher dans la table arp l'adresse IP de ma passerelle comme je la connais deja grace au ipconfig du debut (10.33.3.253) et ensuite une fois qu'on a trouver l'adresse IP bah on a trouver l'adresse MAC aussi (adresse MAC = 00-12-00-40-4c-bf)

```
{...}
Interface : 10.33.3.151 --- 0xd
  Adresse Internet      Adresse physique      Type
  10.33.1.3             94-e6-f7-95-6a-87     dynamique
  10.33.1.41            9c-fc-e8-36-7b-ee     dynamique
  10.33.1.100           e4-5e-37-d2-85-25     dynamique
  10.33.2.27            3c-58-c2-ec-71-33     dynamique
  10.33.2.48            80-91-33-9c-bf-0d     dynamique
  10.33.2.199           c8-e2-65-69-56-a4     dynamique
  10.33.2.214           d8-c0-a6-41-ea-49     dynamique
  10.33.3.103           84-1b-77-96-85-02     dynamique
  10.33.3.120           3c-6a-a7-c3-0a-28     dynamique
  10.33.3.139           34-cf-f6-37-2c-fb     dynamique
  10.33.3.249           58-96-1d-17-43-f5     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  {...}
```

- **Et si on remplissait un peu la table ?**
```
PS C:\Users\marti> ping 10.33.3.6

Envoi d’une requête 'Ping'  10.33.3.6 avec 32 octets de données :
Délai d’attente de la demande dépassé.

Statistiques Ping pour 10.33.3.6:
    Paquets : envoyés = 1, reçus = 0, perdus = 1 (perte 100%),
Ctrl+C

PS C:\Users\marti> ping 10.33.3.103

Envoi d’une requête 'Ping'  10.33.3.103 avec 32 octets de données :
Délai d’attente de la demande dépassé.

Statistiques Ping pour 10.33.3.103:
    Paquets : envoyés = 1, reçus = 0, perdus = 1 (perte 100%),
Ctrl+C

PS C:\Users\marti> ping 10.33.3.120

Envoi d’une requête 'Ping'  10.33.3.120 avec 32 octets de données :
Délai d’attente de la demande dépassé.

Statistiques Ping pour 10.33.3.120:
    Paquets : envoyés = 1, reçus = 0, perdus = 1 (perte 100%),
Ctrl+C

PS C:\Users\marti> ping 10.33.3.139

Envoi d’une requête 'Ping'  10.33.3.139 avec 32 octets de données :
Délai d’attente de la demande dépassé.

Statistiques Ping pour 10.33.3.139:
    Paquets : envoyés = 1, reçus = 0, perdus = 1 (perte 100%),
Ctrl+C
```
Voici les adresse MAC des IP que j'ai ping
```
{...}
  Adresse Internet      Adresse physique      Type
  10.33.3.6             f0-9e-4a-67-dd-c4     dynamique
  10.33.3.103           84-1b-77-96-85-02     dynamique
  10.33.3.120           3c-6a-a7-c3-0a-28     dynamique
  10.33.3.139           34-cf-f6-37-2c-fb     dynamique
 {...} 
```

#### C. nmap

- **Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre**

![](https://i.imgur.com/IQVWVei.png)


```
{...}
Interface : 10.33.3.151 --- 0xd
  Adresse Internet      Adresse physique      Type
  10.33.0.6             84-5c-f3-80-32-07     dynamique
  10.33.0.42            ce-a6-a5-8c-f3-7a     dynamique
  10.33.0.180           26-91-29-98-e2-9d     dynamique
  10.33.1.139           70-66-55-a1-b0-19     dynamique
  10.33.1.205           e8-84-a5-31-6a-be     dynamique
  10.33.2.36            82-f8-ae-af-4f-a6     dynamique
  10.33.2.65            68-ec-c5-42-af-e5     dynamique
  10.33.2.88            68-3e-26-7c-c3-1a     dynamique
  10.33.2.144           ec-63-d7-b6-df-8c     dynamique
  10.33.2.196           08-71-90-87-b9-8c     dynamique
  10.33.2.199           c8-e2-65-69-56-a4     dynamique
  10.33.3.23            7e-7e-4e-bb-23-88     dynamique
  10.33.3.100           2a-84-55-56-76-ea     dynamique
  10.33.3.175           78-4f-43-85-a1-17     dynamique
  10.33.3.179           94-e7-0b-0a-46-aa     dynamique
  10.33.3.207           c0-e4-34-1b-fe-a9     dynamique
  10.33.3.212           f8-5e-a0-17-91-ed     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  {...}
```

#### D. Modification d'adresse IP (part 2)

- **Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap**

![](https://i.imgur.com/RyvFm9X.png)

```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::5067:6567:fb95:d313%13
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.34
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```

```
PS C:\Users\marti> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=47 ms TTL=110
Réponse de 8.8.8.8 : octets=32 temps=37 ms TTL=110
Réponse de 8.8.8.8 : octets=32 temps=52 ms TTL=110
Réponse de 8.8.8.8 : octets=32 temps=43 ms TTL=110

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 37ms, Maximum = 52ms, Moyenne = 44ms
```

### 3. Modification d'adresse IP

![](https://i.imgur.com/dxNaayl.png)


```

PS C:\Users\marti> ipconfig

{...}
Carte Ethernet Ethernet :

   Adresse physique . . . . . . . . . . . : 08-97-98-94-9F-57
   DHCP activé. . . . . . . . . . . . . . : Non
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.1(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   {...}
```

```
PS C:\Users\marti> ping 192.168.0.2

Envoi d’une requête 'Ping'  192.168.0.2 avec 32 octets de données :
Réponse de 192.168.0.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 192.168.0.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms

```

```
PS C:\Users\marti> arp -a

{...}
Interface : 192.168.0.1 --- 0xa
  Adresse Internet      Adresse physique      Type
  169.254.197.33        bc-5f-f4-38-94-ff     dynamique
  192.168.0.2           bc-5f-f4-38-94-ff     dynamique
  192.168.0.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
  {...}

```

### 4. Utilisation d'un des deux comme gateway

![](https://i.imgur.com/kWZCVZt.png)


```
PS C:\Users\marti> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=47 ms TTL=110
Réponse de 8.8.8.8 : octets=32 temps=37 ms TTL=110
Réponse de 8.8.8.8 : octets=32 temps=52 ms TTL=110
Réponse de 8.8.8.8 : octets=32 temps=43 ms TTL=110

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 37ms, Maximum = 52ms, Moyenne = 44ms
    
PS C:\Users\marti> ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=35 ms TTL=53
Réponse de 1.1.1.1 : octets=32 temps=42 ms TTL=53
Réponse de 1.1.1.1 : octets=32 temps=34 ms TTL=53
Réponse de 1.1.1.1 : octets=32 temps=42 ms TTL=53

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 34ms, Maximum = 42ms, Moyenne = 38ms
```

```
PS C:\WINDOWS\system32> tracert google.com

Détermination de l’itinéraire vers google.com [142.250.178.142]
avec un maximum de 30 sauts :

  1    <1 ms     *       <1 ms  MartialPC [192.168.0.2]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     6 ms     4 ms     2 ms  192.168.1.1
  4    89 ms    74 ms    71 ms  10.125.51.5
  5    39 ms    27 ms    37 ms  10.125.127.82
  6    40 ms    27 ms    37 ms  10.151.14.50
  7   113 ms    52 ms    79 ms  89.89.101.56
  8    39 ms    53 ms    49 ms  62.34.2.169
  9    53 ms    27 ms    38 ms  be5.cbr01-ntr.net.bbox.fr [212.194.171.137]
 10     *        *        *     Délai d’attente de la demande dépassé.
 11    36 ms    37 ms    36 ms  72.14.204.68
 12    32 ms    47 ms    31 ms  108.170.245.1
 13    48 ms    34 ms    35 ms  142.251.64.131
 14   308 ms   532 ms   191 ms  par21s22-in-f14.1e100.net [142.250.178.142]

Itinéraire déterminé.
```

### 5. Petit chat privé

![](https://i.imgur.com/H2GIBsb.png)

![](https://i.imgur.com/CPt4h6H.png)

- **pour aller un peu plus loin**

![](https://i.imgur.com/fD13L0A.png)

![](https://i.imgur.com/WPwfuvm.png)


### 6. Firewall

propriété de la règle pour laisser passer les ping

![](https://i.imgur.com/XAILIUa.png)

propriété de la règle pour laisser passer les trames tcp

![](https://i.imgur.com/ZUZ7iWn.png)

![](https://i.imgur.com/e6yJfjR.png)

petit screen pour montrer que ça fonctionne sans problème

![](https://i.imgur.com/AKPlIb5.png)


![](https://i.imgur.com/Gf7EiIt.png)

![](https://i.imgur.com/CW5P3CI.png)



## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP

```
C:\Cours\Cours_B2\Linux\netcat>ipconfig /all

{...}
Carte réseau sans fil Wi-Fi :

   {...}
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   {...}
   Bail obtenu. . . . . . . . . . . . . . : vendredi 17 septembre 2021 13:43:20
   Bail expirant. . . . . . . . . . . . . : samedi 18 septembre 2021 13:43:34
   Passerelle par défaut. . . . . . . . . : 192.168.1.1
   Serveur DHCP . . . . . . . . . . . . . : 192.168.1.1
   {...}
```

### 2. DNS

```
C:\Cours\Cours_B2\Linux\netcat>ipconfig /all

{...}
Carte réseau sans fil Wi-Fi :

{...}
   Serveurs DNS. . .  . . . . . . . . . . : 192.168.1.1
{...}
```

```
C:\Cours\Cours_B2\Linux\netcat>nslookup google.com
Serveur :   Bbox.nomad
Address:  192.168.1.1

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:812::200e
          216.58.213.78
```

```
C:\Cours\Cours_B2\Linux\netcat>nslookup ynov.com
Serveur :   Bbox.nomad
Address:  192.168.1.1

Réponse ne faisant pas autorité :
Nom :    ynov.com
Addresses:  64:ff9b::5cf3:108f
          92.243.16.143
```
on peut constater que pour obtenir l'adresse ip de google.com ou ynov.com il passe par le serveur qui est ma box avec comme adresse ip celle du dns donc il utilise mon serveur dns pour recupere l'adresse ip de google.com


```
C:\Cours\Cours_B2\Linux\netcat>nslookup google.com
Serveur :   Bbox.nomad
Address:  192.168.1.1
```
l'adresse ip a qui on fait la requete est celle du serveur dns

```
C:\Cours\Cours_B2\Linux\netcat>nslookup 78.74.21.21
Serveur :   Bbox.nomad
Address:  192.168.1.1

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21

```
```

C:\Cours\Cours_B2\Linux\netcat>nslookup 92.146.54.88
Serveur :   Bbox.nomad
Address:  192.168.1.1

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```
on peut constater que pour recuperé le nom d'hote a partir de l'adresse ip nslookup passe par ma box comme serveur avec comme adreese ip le dns

## IV. Wireshark

- **ping**

![](https://i.imgur.com/rb72PJA.png)
![](https://i.imgur.com/SM4AwoZ.png)
![](https://i.imgur.com/EQL5HdB.png)
![](https://i.imgur.com/ksG3YEn.png)

- **netcat**

![](https://i.imgur.com/mSD1PdW.png)
![](https://i.imgur.com/G0gdoVi.png)
![](https://i.imgur.com/nwQyPD8.png)
![](https://i.imgur.com/TIhv6j9.png)
![](https://i.imgur.com/6CrpdKT.png)
![](https://i.imgur.com/k5moMxR.png)
![](https://i.imgur.com/dHgBcJU.png)

- **DNS**

![](https://i.imgur.com/KX8l2U9.png)
![](https://i.imgur.com/tnC8x5M.png)
![](https://i.imgur.com/pRl2ex4.png)
![](https://i.imgur.com/udmNgUU.png)






