# TP4 : Vers un réseau d'entreprise
# I. Dumb switch
## 3. Setup topologie 1

🌞 **Commençons simple**

- définissez les IPs statiques sur les deux VPCS

```
PC1> ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0
```

```
PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0
```
- `ping` un VPCS depuis l'autre

```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=4.165 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.054 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=5.180 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=3.346 ms
^C
```

```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=2.491 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=1.571 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=1.786 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=3.000 ms
^C
```

# II. VLAN
### 3. Setup topologie 2

🌞 **Adressage**

- définissez les IPs statiques sur tous les VPCS

```
PC1> ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0
```

```
PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0
```

```
PC3> ip 10.1.1.3/24
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0
```

- vérifiez avec des `ping` que tout le monde se ping

```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=4.165 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.054 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=5.180 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=3.346 ms
^C
```

```
PC1> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=2.100 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=2.372 ms
84 bytes from 10.1.1.3 icmp_seq=3 ttl=64 time=3.893 ms
^C
```

```
PC2> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=2.100 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=2.372 ms
84 bytes from 10.1.1.3 icmp_seq=3 ttl=64 time=3.893 ms
^C
```

```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=2.491 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=1.571 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=1.786 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=3.000 ms
^C
```

```
PC3> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=2.976 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=2.948 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=2.617 ms
^C

```

```
PC3> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=5.339 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=4.258 ms
^C
```

🌞 **Configuration des VLANs**

```
Switch>enable
Switch#
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#
Switch(config)#vlan 10
Switch(config-vlan)#
Switch(config-vlan)#name admins
Switch(config-vlan)#
Switch(config-vlan)#exit
Switch(config)#
Switch(config)#vlan 20
Switch(config-vlan)#
Switch(config-vlan)#name guests
Switch(config-vlan)#
Switch(config-vlan)#exit
```

```
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#
Switch(config)#interface GigabitEthernet 0/0
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#no shutdown
Switch(config-if)#exit
Switch(config)#
Switch(config)#interface GigabitEthernet 0/1
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#no shutdown
Switch(config-if)#exit
Switch(config)#
Switch(config)#interface GigabitEthernet 0/0
Switch(config-if)#no shutdown
Switch(config-if)#exit
Switch(config)#
Switch(config)#interface GigabitEthernet 0/2
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 20
Switch(config-if)#no shutdown
Switch(config-if)#exit
Switch#
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/3, Gi1/0, Gi1/1, Gi1/2
                                                Gi1/3, Gi2/0, Gi2/1, Gi2/2
                                                Gi2/3, Gi3/0, Gi3/1, Gi3/2
                                                Gi3/3
10   admins                           active    Gi0/0, Gi0/1
20   guests                           active    Gi0/2
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup
```

🌞 **Vérif**

- `pc1` et `pc2` doivent toujours pouvoir se ping

```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=2.471 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=2.849 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=2.811 ms
^C
```

```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=2.491 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=1.571 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=1.786 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=3.000 ms
^C
```

- `pc3` ne ping plus personne

```
PC3> ping 10.1.1.1

host (10.1.1.1) not reachable
```

```
PC3> ping 10.1.1.2

host (10.1.1.2) not reachable
```
# III. Routing

Dans cette partie, on va donner un peu de sens aux VLANs :

- un pour les serveurs du réseau
  - on simulera ça avec un p'tit serveur web
- un pour les admins du réseau
- un pour les autres random clients du réseau

Cela dit, il faut que tout ce beau monde puisse se ping, au moins joindre le réseau des serveurs, pour accéder au super site-web.

**Bien que bloqué au niveau du switch à cause des VLANs, le trafic pourra passer d'un VLAN à l'autre grâce à un routeur.**

Il assurera son job de routeur traditionnel : router entre deux réseaux. Sauf qu'en plus, il gérera le changement de VLAN à la volée.

## 1. Topologie 3

![Topologie 3](./pics/topo3.png)

## 2. Adressage topologie 3

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `admins`  | `10.2.2.0/24` | 12           |
| `servers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 3

🖥️ VM `web1.servers.tp4`, déroulez la [Checklist VM Linux](#checklist-vm-linux) dessus

🌞 **Adressage**

- définissez les IPs statiques sur toutes les machines **sauf le *routeur***

```
PC1> ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0
```

```
PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0
```

```
adm1> ip 10.2.2.1/24
Checking for duplicate address...
adm1 : 10.2.2.1 255.255.255.0
```

```
[admin@web1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8

NAME=enp0s8
DEVICE=enp0s8

IPADDR=10.3.3.1
NETMASK=255.255.255.0

BOOTPROTO=static
ONBOOT=yes
```

🌞 **Configuration des VLANs**

```
Switch>en
Switch#conf t
Switch(config)#interface gigabitEthernet 0/0
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 11
Switch(config-if)#
Switch(config-if)#no shutdown
Switch(config-if)#
Switch(config-if)#exit
```
```
Switch(config)#interface gigabitEthernet 0/1
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 11
Switch(config-if)#
Switch(config-if)#no shutdown
Switch(config-if)#
Switch(config-if)#exit
```
```
Switch(config)#interface gigabitEthernet 0/2
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 12
Switch(config-if)#
Switch(config-if)#no shutdown
Switch(config-if)#
Switch(config-if)#exit
```
```
Switch(config)#interface gigabitEthernet 0/3
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 13
Switch(config-if)#
Switch(config-if)#no shutdown
Switch(config-if)#
Switch(config-if)#exit
```
```
Switch(config)#interface gigabitEthernet 1/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#
Switch(config-if)#switchport trunk allowed vlan 11,12,13
Switch(config-if)#
Switch(config-if)#no shutdown
Switch(config-if)#
Switch(config-if)#exit
```
```
Switch# show interface trunk
Switch#
Port        Mode             Encapsulation  Status        Native vlan
Gi1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/0       11-13

Port        Vlans allowed and active in management domain
Gi1/0       11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/0       11-13
Switch#
```
```
Switch#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi1/1, Gi1/2, Gi1/3, Gi2/0
                                                Gi2/1, Gi2/2, Gi2/3, Gi3/0
                                                Gi3/1, Gi3/2, Gi3/3
11   clients                          active    Gi0/0, Gi0/1
12   admins                           active    Gi0/2
13   servers                          active    Gi0/3
```

🌞 **Config du *routeur***

```
R1(config)# interface fastEthernet 0/0.11
R1(config-subif)# encapsulation dot1Q 11
R1(config-subif)# ip addr 10.1.1.254 255.255.255.0
R1(config-subif)# no shutdown
R1(config-subif)# exit

```

```
R1(config)# interface fastEthernet 0/0.12
R1(config-subif)# encapsulation dot1Q 12
R1(config-subif)# ip addr 10.2.2.254 255.255.255.0
R1(config-subif)# no shutdown
R1(config-subif)# exit

```
```
R1(config)# interface fastEthernet 0/0.13
R1(config-subif)# encapsulation dot1Q 13
R1(config-subif)# ip addr 10.3.3.254 255.255.255.0
R1(config-subif)# no shutdown
R1(config-subif)# exit

```

```
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES NVRAM  up                    up
FastEthernet0/0.11         10.1.1.254      YES NVRAM  up                    up
FastEthernet0/0.12         10.2.2.254      YES NVRAM  up                    up
FastEthernet0/0.13         10.3.3.254      YES NVRAM  up                    up
```

🌞 **Vérif**

- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau

```
PC1> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=61.755 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=24.632 ms
^C
```

```
PC2> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=61.755 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=24.632 ms
^C
```

```
adm1> ping 10.2.2.254

84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=21.584 ms
84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=13.159 ms
^C
```

```
[admin@web1 ~]$ ping 10.3.3.254

84 bytes from 10.3.3.254 icmp_seq=1 ttl=255 time=21.584 ms
84 bytes from 10.3.3.254 icmp_seq=2 ttl=255 time=13.159 ms
^C
```

- en ajoutant une route vers les réseaux, ils peuvent se ping entre eux
  - ajoutez une route par défaut sur les VPCS
  - ajoutez une route par défaut sur la machine virtuelle
  - testez des `ping` entre les réseaux

# IV. NAT
## 3. Setup topologie 4

🌞 **Ajoutez le noeud Cloud à la topo**

- côté routeur, il faudra récupérer un IP en DHCP

```
R1(config)#interface f1/0
R1(config-if)#ip address dhcp
R1(config-if)#no shutdown
R1(config-if)#exit

```
- vous devriez pouvoir `ping 1.1.1.1`

```
PC1> ping 1.1.1.1

1.1.1.1 icmp_seq=1 timeout
84 bytes from 1.1.1.1 icmp_seq=2 ttl=56 time=56.819 ms
84 bytes from 1.1.1.1 icmp_seq=3 ttl=56 time=39.206 ms
84 bytes from 1.1.1.1 icmp_seq=4 ttl=56 time=49.701 ms
84 bytes from 1.1.1.1 icmp_seq=5 ttl=56 time=48.392 ms

```

🌞 **Configurez le NAT**

```
R1(config)#interface f0/0
R1(config-if)#ip nat inside
R1(config-if)#no shutdown
R1(config-if)#exit
```
```
R1(config)#interface f1/0
R1(config-if)#ip nat outside
R1(config-if)#no shutdown
R1(config-if)#exit
```
```
R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface fastEthernet 0/0 overload
```

🌞 **Test**

- ajoutez une route par défaut (si c'est pas déjà fait)

```
PC1> ip 10.1.1.1 10.1.1.254
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0 gateway 10.1.1.254
```

```
PC2> ip 10.1.1.2 10.1.1.254
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254
```

```
adm1> ip 10.2.2.1 10.2.2.254
Checking for duplicate address...
adm1 : 10.2.2.1 255.255.255.0 gateway 10.2.2.254
```

```
web1
GATEWAY=10.3.3.254
```

- configurez l'utilisation d'un DNS

```
PC1> ip dns 1.1.1.1
```

```
PC2> ip dns 1.1.1.1
```

```
adm1> ip dns 1.1.1.1
```

```
[admin@web1 ~]$ sudo nano /etc/resolv.conf

nameserver 1.1.1.1
```
- vérifiez un `ping` vers un nom de domaine

```
PC1> ping google.com
google.com resolved to 142.250.179.78

84 bytes from 142.250.179.78 icmp_seq=1 ttl=112 time=40.873 ms
84 bytes from 142.250.179.78 icmp_seq=2 ttl=112 time=45.031 ms
```